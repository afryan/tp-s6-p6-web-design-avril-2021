<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('production');
	}
	public function index($id_section, $id)
	{
		$this->load->model('PostModel');
		$data['post_section'] = $this->PostModel->find_all_post_section_paragraphs_by_ids($id, $id_section);
		$data['post'] = $this->PostModel->find_post_by_ids($id, $id_section);
		$data['recent_post'] = $this->PostModel->find_recent_post($id);
		$this->load->view('post', $data);
	}
	public function New()
	{
		session_start();
		if (!isset($_SESSION['id'])) {
			$message = "Veuillez vous connecter.";
			redirect('Auth?message=' . $message);
		} else {
			$this->load->model('AuthModel');
			$user = $this->AuthModel->find_user_by_id($_SESSION['id']);
			if (isset($user)) {
				$data['user'] = $user;
				$data['front_office'] = $this->production->front_url();
				$this->load->model('SectionModel');
				$data['upload_message'] = !isset($_GET['upload_message']) ? ''  : $_GET['upload_message'];
				$data['sections'] = $this->SectionModel->find_all_section();
				$this->load->view('post_form', $data);
			} else {
				$message = "Veuillez vous connecter.";
				redirect('Auth?message=' . $message);
			}
		}
	}
	public function New_Action()
	{
		session_start();
		if (!isset($_SESSION['id'])) {
			$message = "Veuillez vous connecter.";
			redirect('Auth?message=' . $message);
		} else {
			$this->load->model('AuthModel');
			$user = $this->AuthModel->find_user_by_id($_SESSION['id']);
			if (isset($user)) {
				$this->load->model('PostModel');
				$input = $this->input->post();
				$id_section = $input['id_section'];
				$title = $input['title'];
				$author = $input['author'];
				$strong_words = $input['strong_words'];
				$chapo = $input['chapo'];
				$image_name = '';
				if ($_FILES["background_image"]["name"] != '') {
					/* BEGIN Image Upload */
					$target_dir = "assets/img/";
					$upload_message = "";
					date_default_timezone_set('UTC');
					$image_name = date_timestamp_get(new DateTime('NOW')) . '-' . basename($_FILES["background_image"]["name"]);
					$target_file = $target_dir  . $image_name;
					$uploadOk = 1;
					$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
					// File is an image
					$check = getimagesize($_FILES["background_image"]["tmp_name"]);
					if ($check !== false) {
						$uploadOk = 1;
					} else {
						$uploadOk = 0;
						$upload_message = "Le fichier n'est pas une image.";
					}
					// File Already Exist
					if (file_exists($target_file)) {
						$uploadOk = 0;
						$upload_message = "L'image est déjà enregistrée sur notre serveur.";
					}
					if ($_FILES["background_image"]["size"] > 500000) {
						$uploadOk = 0;
						$upload_message = "Le fichier est trop volumineux et doit être inférieur à 500 ko.";
					}
					if (
						$imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
					) {
						$uploadOk = 0;
						$upload_message = "L'image doit être au format .jpg/.jpeg ou .png";
					}
					if ($uploadOk == 0) {
						redirect('Post/New?upload_message=' . $upload_message);
					} else {
						move_uploaded_file($_FILES["background_image"]["tmp_name"], $target_file);
					}
				}
				/* END Image Upload */

				$background_url = $image_name;
				$background_copyright = $input['background_copyright'];

				$this->PostModel->create_post($id_section, $title, $author, $strong_words, $chapo, $background_url, $background_copyright);
				$last = $this->PostModel->find_last_post();
				redirect('Post/List/' . $last['id']);
			} else {
				$message = "Veuillez vous connecter.";
				redirect('Auth?message=' . $message);
			}
		}
	}
	public function List($id = 0, $id_post_section = 0, $id_paragraph = 0)
	{
		session_start();
		if (!isset($_SESSION['id'])) {
			$message = "Veuillez vous connecter.";
			redirect('Auth?message=' . $message);
		} else {
			$this->load->model('AuthModel');
			$user = $this->AuthModel->find_user_by_id($_SESSION['id']);
			if (isset($user)) {
				$data['user'] = $user;
				$data['front_office'] = $this->production->front_url();
				$this->load->model('PostModel');
				if ($id != 0) {
					if ($id_post_section == 0 && $id_paragraph == 0) {
						if (isset($_GET['add']) && $_GET['add'] = 'true') {
							$data['id_post'] = $id;
							$this->load->view('post_section_add', $data);
						} else {
							$data['post'] = $this->PostModel->find_post_by_id($id);
							$data['post_section'] = $this->PostModel->find_all_post_section_paragraphs_by_id($id);
							if (isset($data['post'])) {
								$this->load->view('post_card', $data);
							} else {
								$error['heading'] = "404";
								$error['message'] = "Page non trouvée";
								$error['recent_post'] = $this->PostModel->find_all_recent_post(3);
								$this->load->view('errors/html/back-error_404', $error);
							}
						}
					} else {
						if ($id_post_section != 0 && $id_paragraph == 0) {
							if (isset($_GET['add']) && $_GET['add'] = 'true') {
								$data['id_post_section'] = $id_post_section;
								$data['id_post'] = $id;
								$this->load->view('post_paragraph_add', $data);
							} else {
								$data['id_post'] = $id;
								$data['post_section'] = $this->PostModel->find_post_section_by_id($id_post_section);
								$this->load->view('post_section_card', $data);
							}
						} else if ($id_post_section != 0 && $id_paragraph != 0) {
							$data['id_post'] = $id;
							$data['paragraph'] = $this->PostModel->find_post_paragraph_by_id($id_paragraph);
							$this->load->view('post_paragraph_card', $data);
						}
					}
				} else {
					$input = $this->input->get();
					$limit = !isset($input['limit']) ? 10 : $input['limit'];
					$page = !isset($input['page']) ? 1 : $input['page'];
					$data['posts'] = $this->PostModel->find_all_post_by_date_desc($page, $limit);
					$this->load->view('post_list', $data);
				}
			} else {
				$message = "Veuillez vous connecter.";
				redirect('Auth?message=' . $message);
			}
		}
	}
	public function Delete($id_post)
	{
		$this->load->model('PostModel');
		$this->PostModel->delete_post_rec($id_post);
		redirect('Post/List');
	}
	public function Update($id_post)
	{
		session_start();
		if (!isset($_SESSION['id'])) {
			$message = "Veuillez vous connecter.";
			redirect('Auth?message=' . $message);
		} else {
			$this->load->model('AuthModel');
			$user = $this->AuthModel->find_user_by_id($_SESSION['id']);
			if (isset($user)) {
				$data['user'] = $user;
				$this->load->model('PostModel');
				$this->load->model('SectionModel');
				$data['front_office'] = $this->production->front_url();
				$data['upload_message'] = !isset($_GET['upload_message']) ? ''  : $_GET['upload_message'];
				$data['sections'] = $this->SectionModel->find_all_section();
				$data['post'] = $this->PostModel->find_post_by_id($id_post);
				$this->load->view('post_form_update', $data);
			} else {
				$message = "Veuillez vous connecter.";
				redirect('Auth?message=' . $message);
			}
		}
	}
	public function Update_Action()
	{
		$this->load->model('PostModel');
		$input = $this->input->post();
		$id = $input['id'];
		$id_section = $input['id_section'];
		$title = $input['title'];
		$author = $input['author'];
		$strong_words = $input['strong_words'];
		$chapo = $input['chapo'];
		$background_copyright = $input['background_copyright'];

		$this->PostModel->update_post($id, $id_section, $title, $author, $strong_words, $chapo, $background_copyright);
		redirect('Post/List/' . $id);
	}
}
