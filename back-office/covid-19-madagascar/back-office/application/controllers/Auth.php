<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		session_start();
	}
	public function index()
	{
		if (isset($_SESSION['id'])) {
			$this->load->model('AuthModel');
			$user = $this->AuthModel->find_user_by_id($_SESSION['id']);
			if (isset($user)) {
				redirect('Post/List');
			}
		} else {
			$this->load->library('Production');
			$data['front_office'] = $this->production->front_url();
			$data['message'] = isset($_GET['message']) ? $_GET['message'] : '';
			$this->load->view('login', $data);
		}
	}
	public function Login()
	{
		if (isset($_SESSION['id'])) {
			$this->load->model('AuthModel');
			$user = $this->AuthModel->find_user_by_id($_SESSION['id']);
			if (isset($user)) {
				redirect('Post/List');
			}
		} else {
			$username = $_POST['username'];
			$password = $_POST['password'];
			$this->load->model('AuthModel');
			$id = $this->AuthModel->login($username, $password);
			if (isset($id)) {
				session_start();
				$_SESSION['id'] = $id;
				redirect('Post/List');
			} else {
				$message = 'Identifiant/Mot de passe incorect.';
				redirect('Auth?message=' . $message);
			}
		}
	}
	public function Logout()
	{
		session_start();
		$_SESSION = array();
		session_destroy();

		redirect('Auth');
	}
	public function Register()
	{
		if (isset($_SESSION['id'])) {
			$this->load->model('AuthModel');
			$user = $this->AuthModel->find_user_by_id($_SESSION['id']);
			if (isset($user)) {
				redirect('Post/List');
			}
		} else {
			$this->load->library('Production');
			$data['front_office'] = $this->production->front_url();
			$this->load->view('register', $data);
		}
	}
	public function Register_Action()
	{
		if (isset($_SESSION['id'])) {
			$this->load->model('AuthModel');
			$user = $this->AuthModel->find_user_by_id($_SESSION['id']);
			if (isset($user)) {
				redirect('Post/List');
			}
		} else {
			$this->load->model('AuthModel');
			$i = $this->input->post();
			$email = $i['email'];
			$username = $i['username'];
			$password = $i['password'];
			$this->AuthModel->register($email, $username, $password);
			$message = 'Compte créé - Vous pouvez vous connecter.';
			redirect('Auth?message=' . $message);
		}
	}
}
