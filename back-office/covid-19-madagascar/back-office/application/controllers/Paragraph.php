<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Paragraph extends CI_Controller
{

	public function Add()
	{
		$this->load->model('PostModel');
		$id_post_section = $_POST['id_post_section'];
		$id_post = $_POST['id_post'];
		$content = $_POST['content'];
		$this->PostModel->create_post_paragraph($id_post_section, $content);
		redirect('Post/List/' . $id_post);
	}
	public function Update()
	{
		$this->load->model('PostModel');
		$id_post = $_POST['id_post'];
		$id = $_POST['id_paragraph'];
		$content = $_POST['content'];
		$this->PostModel->update_post_paragraph($id, $content);
		redirect('Post/List/' . $id_post);
	}
	public function Delete($id_post, $id_paragraph)
	{
		$this->load->model('PostModel');
		$this->PostModel->delete_paragraph($id_paragraph);
		redirect('Post/List/' . $id_post);
	}
}
