<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PostSection extends CI_Controller
{
    public function Add()
    {
        $this->load->model('PostModel');
        $id_post = $_POST['id_post'];
        $title = $_POST['title'];
        $this->PostModel->create_post_section($id_post, $title);
        redirect(site_url('Post/List/' . $id_post));
    }

    public function Update()
    {
        $this->load->model('PostModel');
        $id_post = $_POST['id_post'];
        $title = $_POST['title'];
        $id = $_POST['id_post_section'];
        $this->PostModel->update_post_section($id, $title);
        redirect(site_url('Post/List/' . $id_post));
    }
    public function Delete($id_post,$id)
    {
        $this->load->model('PostModel');
        $this->PostModel->delete_post_section_rec($id);
        redirect(site_url('Post/List/' . $id_post));
    }
}
