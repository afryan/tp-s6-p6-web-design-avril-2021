<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Production
{
    public function front_url()
    {
        return "https://covid-19-fo.alwaysdata.net/";
    }
    public function back_url(){
        return "https://covid-19-bo.alwaysdata.net/";
    }
}
