<?php
defined('BASEPATH') or exit('No direct script access allowed');
class AuthModel extends CI_Model
{
    public function login($username, $password)
    {
        $sql = sprintf("SELECT id FROM User WHERE username = '%s' AND password = sha1('%s')", $username, $password);
        $res = $this->db->query($sql)->row_array();
        return $res['id'];
    }

    public function register($email, $username, $password)
    {
        $sql = sprintf("INSERT INTO User (email,username,password) VALUES ('%s','%s',sha1('%s'))", $email, $username, $password);
        return $this->db->query($sql);
    }
    public function find_user_by_id($id)
    {
        return $this->db->get_where('User', ['id' => $id])->row_array();
    }
}
