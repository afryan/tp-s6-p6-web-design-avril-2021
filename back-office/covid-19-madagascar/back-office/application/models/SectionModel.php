<?php
defined('BASEPATH') or exit('No direct script access allowed');
class SectionModel  extends CI_Model
{
    public function find_all_section(){
        return $this->db->get('Section')->result_array();
    }
}