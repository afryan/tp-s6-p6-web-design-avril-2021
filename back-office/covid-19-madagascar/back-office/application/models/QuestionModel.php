<?php
defined('BASEPATH') or exit('No direct script access allowed');
class QuestionModel  extends CI_Model
{
    public function find_question_response()
    {
        return $this->db->get('Question')->result_array();
    }
}
