<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="1er site d'actualité spécialisées dans la COVID-19 à Madagascar.">
	<link rel="icon" type="image/svg" href="../../assets/img/virus.svg" />

	<meta name="author" content="ETU001068">

	<title><?= $message ?></title>
	<!-- Bootstrap core CSS -->
	<link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom fonts for this template -->
	<link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

	<!-- Custom styles for this template -->
	<link href="../../assets/css/clean-blog.min.css" rel="stylesheet">
</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:rgb(49, 48, 48);" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="<?= base_url() ?>" style="padding: 5px; color: #fff;">
                <img src="../../assets/img/virus.svg" width="40px" height="40px" alt="bacteria">
                <span style="color:brown">
                    ALERTE </span>COVID-19</a>
            <button class="navbar-toggler navbar-toggler-right" style="color:#fff" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="#">Site Front-office</a>
                    </li>
                    <li class="nav-item" style="margin-right: 5px;">
                        <a href="<?=site_url('Login')?>" class="btn btn-primary full">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-lock" viewBox="0 0 16 16">
                                <path d="M8 5a1 1 0 0 1 1 1v1H7V6a1 1 0 0 1 1-1zm2 2.076V6a2 2 0 1 0-4 0v1.076c-.54.166-1 .597-1 1.224v2.4c0 .816.781 1.3 1.5 1.3h3c.719 0 1.5-.484 1.5-1.3V8.3c0-.627-.46-1.058-1-1.224zM6.105 8.125A.637.637 0 0 1 6.5 8h3a.64.64 0 0 1 .395.125c.085.068.105.133.105.175v2.4c0 .042-.02.107-.105.175A.637.637 0 0 1 9.5 11h-3a.637.637 0 0 1-.395-.125C6.02 10.807 6 10.742 6 10.7V8.3c0-.042.02-.107.105-.175z" />
                                <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z" />
                            </svg>
                            Se connecter</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('Register') ?>" class="btn btn-warning full">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-text" viewBox="0 0 16 16">
                                <path d="M5 4a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm-.5 2.5A.5.5 0 0 1 5 6h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zM5 8a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm0 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1H5z" />
                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z" />
                            </svg> S'inscrire
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
	<!-- Main Content -->
	<div class="container ">
		<div class="row ">
			<div class="col-lg-8 col-md-10 mx-auto" style="margin-top: 120px ">
				<p class="text-center"><img src="../../assets/img/error-404.png" alt="error-404" width="200px" height="200px"></p>
				<h3 style="text-align:center" class="error"><?= $message ?></h3>
				<hr>
				<h3>Voir aussi ...</h3>
				<?php for ($i = 0; $i < count($recent_post); $i++) { ?>
					<div class="post-preview ">
						<a href="<?= site_url(('/' . $recent_post[$i]['post_url'])) ?>" class="post">
							<div class="post-category"><?= $recent_post[$i]['section_name'] ?></div>

							<h2 class="post-title">
								<?= $recent_post[$i]['title'] ?>
							</h2>
							<h3 class="post-subtitle ">
								<?= $recent_post[$i]['chapo'] ?>
							</h3>
						</a>
					</div>

				<?php if ($i != count($recent_post) - 1) {
						echo '<hr>';
					}
				} ?>
			</div>
		</div>
	</div>
	<footer style="background-color: rgb(49, 48, 48); ">
		<div class="container ">
			<div class="row ">
				<div class="col-lg-8 col-md-10 mx-auto ">
					<p class="copyright" style="color: #fff;">Copyright &copy; ETU001068 - Andriamahery Fihariantsoa Ryan <br>Images & icons credits : flaticon.com</p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Bootstrap core JavaScript -->
	<script src="../../vendor/jquery/jquery.min.js "></script>
	<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js "></script>

	<!-- Custom scripts for this template -->
	<script src="../../assets/js/clean-blog.min.js "></script>

</body>

</html>