<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/svg" href="../assets/img/virus.svg" />
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alerte COVID-19 - Nouveau Post</title>
    <?php include 'post-style-links.php'; ?>

</head>

<body>

    <?php include 'post-nav-bar.php'; ?>
    <div class="container">

        <div style="margin-top: 100px;margin-bottom:250px; height:100vh;">
            <div class="container">
                <?php if ($upload_message != '') { ?>
                    <div class="message-banner">
                        <p><?= $upload_message ?></p>
                    </div>
                <?php } ?>
            </div>
            <div class="row ">
                <div class="col-lg-4 col-md-8 col-sm-8 col-10 mx-auto ">
                    <h1 class="text-center">Formulaire Post</h1>
                    <div style="font-size: smaller;font-style:italic;text-align:center">Tous les champs avec (<span class="required">*</span>) sont obligatoires.</div>
                    <hr>
                    <form enctype="multipart/form-data" method="post" action="<?= site_url('Post/New_Action') ?>">
                        <div class="form-group">
                            <label for="id_section">Section<span class="required">*</span></label>
                            <div class="form-"></div>
                            <select class="form-control" name="id_section" id="id_section">
                                <?php foreach ($sections as $s) { ?>
                                    <option value="<?= $s['id'] ?>"><?= $s['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Titre de l'article<span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="title" id="title" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="author">Auteur<span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="author" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="strong_words">Mots clés</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="strong_words">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="chapo">Chapo<span class="required">*</span></label>
                            <div class="input-group">
                                <textarea class="form-control" rows="3" name="chapo" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="background_image">Image d'illustration <br> <span class="indication">(format .png, .jpg, .jpeg uniquement)</span> <br> <span class="indication"> Taille inférieure à 500 ko</span></label>
                            <input accept="image/png image/jpeg image/jpg" type="file" class="form-control-file" id="background_image" name="background_image">
                        </div>
                        <div class="form-group">
                            <label for="background_copyright">Image Copyright</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="background_copyright">
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="form-control btn-success" type="submit">Créer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../assets/js/clean-blog.min.js"></script>

</body>

</html>