<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/svg" href="../../../../assets/img/virus.svg" />
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alerte COVID-19 - Formulaire Paragraphe</title>
    <!-- Bootstrap core CSS -->
    <link href="../../../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../../../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="../../../../assets/css/clean-blog.min.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:rgb(49, 48, 48);" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="<?= base_url() ?>" style="padding: 5px; color: #fff;">
                <img src="../../../../assets/img/virus.svg" width="40px" height="40px" alt="bacteria">
                <span style="color:brown">
                    ALERTE </span>COVID-19</a>
            <button class="navbar-toggler navbar-toggler-right" style="color:#fff" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <?php include 'back-nav-ul.php'; ?>

            </div>
        </div>
    </nav>
    <div style="margin-top: 100px;margin-bottom:50px; height:70vh;">
        <div class="row ">
            <div class="col-lg-4 col-md-8 col-sm-8 col-10 mx-auto ">
                <!-- <div class="form-wrap"> -->
                <h1 class="text-center">Formulaire Paragraphe</h1>
                <div style="font-size: smaller;font-style:italic;text-align:center">Tous les champs avec (<span class="required">*</span>) sont obligatoires.</div>
                <hr>
                <form method="POST" action=" <?= base_url('Paragraph/Update') ?>">
                    <div class="form-group">
                        <input type="hidden" name="id_post_section" value="<?= $paragraph['id_post_section'] ?>">
                        <input type="hidden" name="id_paragraph" value="<?= $paragraph['id'] ?>">
                        <input type="hidden" name="id_post" value="<?= $id_post ?>">

                        <label for="content">Contenu : <span class="required">*</span></label>
                        <div class="input-group">
                            <textarea rows="8" class="form-control" name="content" id="content" required><?= $paragraph['content'] ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="form-control btn-success" type="submit">Valider</button>
                    </div>
                </form>
                <!-- </div> -->
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer style="background-color: rgb(49, 48, 48); ">
        <div class="container ">
            <div class="row ">
                <div class="col-lg-8 col-md-10 mx-auto ">
                    <p class="copyright" style="color: #fff;">Copyright &copy; ETU001068 - Andriamahery Fihariantsoa Ryan <br>Images & icons credits : flaticon.com</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="../../../../vendor/jquery/jquery.min.js"></script>
    <script src="../../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../../../../assets/js/clean-blog.min.js"></script>

</body>

</html>