 <!-- Navigation -->
 <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:rgb(49, 48, 48);" id="mainNav">
 	<div class="container">
 		<a class="navbar-brand" href="<?= base_url() ?>" style="padding: 5px; color: #fff;">
 			<img src="../assets/img/virus.svg" width="40px" height="40px" alt="bacteria">
 			<span style="color:brown">
 				ALERTE </span>COVID-19</a>
 		<button class="navbar-toggler navbar-toggler-right" style="color:#fff" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
 			<i class="fas fa-bars"></i>
 		</button>
 		<div class="collapse navbar-collapse" id="navbarResponsive">
 			<?php include 'back-nav-ul.php'; ?>

 		</div>
 	</div>
 </nav>