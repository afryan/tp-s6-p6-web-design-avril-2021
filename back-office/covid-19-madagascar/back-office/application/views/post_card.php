<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/svg" href="../../assets/img/virus.svg" />

    <meta name="description" content="<?= $post['chapo'] ?>">
    <meta name="author" content="ETU01068">

    <title>Alerte COVID-19 - <?= $post['title'] ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="../../assets/css/clean-blog.min.css" rel="stylesheet">
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:rgb(49, 48, 48);" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="<?= base_url() ?>" style="padding: 5px; color: #fff;">
                <img src="../../assets/img/virus.svg" width="40px" height="40px" alt="bacteria">
                <span style="color:brown">
                    ALERTE </span>COVID-19</a>
            <button class="navbar-toggler navbar-toggler-right" style="color:#fff" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
            <?php include 'back-nav-ul.php'; ?>

            </div>
        </div>
    </nav>


    <!-- Page Header -->
    <header class="masthead" style="background-color:#fff">
        <div class="overlay" style="background-color: #fff;"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="post-heading" style="padding: 100px 0 20px;">
                        <a href="<?= site_url('Post/Update/' . $post['id']) ?>" class="btn btn-warning">Modifier contenu</a>
                        <a href="<?= site_url('Post/Delete/' . $post['id']) ?>" class="btn btn-danger">Supprimer POST</a>
                        <a href="<?= site_url('Post/List/' . $post['id'] . '?add=true') ?>" class="btn btn-success">Ajouter section</a>
                        <h1 class="article-title" style="color:#000;font-weight:900;"><?= $post['title'] ?></h1>
                        <?php if (isset($post['background_url'])) { ?>
                            <img src="../../assets/img/<?= $post['background_url'] ?>" alt="<?= $post['background_url'] ?>" width="100%" height="100%">
                            <?php if (isset($post['background_copyright'])) { ?>
                                <span class="copyright" style="font-size: small; color:gray;">&copy; <?= $post['background_copyright'] ?></span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <h3 class="post-subtitle chapo" style="font-weight:bold;"><?= $post['chapo'] ?></h3>
                </div>
            </div>
        </div>
    </header>

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <?php for ($i = 0; $i < count($post_section); $i++) {
                        for ($j = 0; $j < count($post_section[$i]); $j++) {
                            if ($j == 0) { ?>
                                <div class="post_header">
                                    <h2 class="section-heading"><?= $post_section[$i][$j]['title'] ?></h2>
                                    <a href="<?= site_url('Post/List/' . $post['id'] . '/' . $post_section[$i][$j]['id_section']) ?>" class="btn btn-dark">Modifier section</a>
                                    <a href="<?= site_url('PostSection/Delete/' . $post['id'] . '/' . $post_section[$i][$j]['id_section']) ?>" class="btn btn-danger">Supprimer section</a>
                                    <?php if (!isset($post_section[$i][$j]['id_paragraph'])) { ?>
                                        <a href="<?= site_url('Post/List/' . $post['id'] . '/' . $post_section[$i][$j]['id_section'] . '?add=true') ?>" class="btn btn-success">Ajouter paragraphe</a>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <p style="font-family:'Open Sans',Arial,sans-serif;"><?= $post_section[$i][$j]['content'] ?></p>
                            <?php if (isset($post_section[$i][$j]['id_paragraph'])) { ?>
                                <a href="<?= site_url('Post/List/' . $post['id'] . '/' . $post_section[$i][$j]['id_section'] . '/' . $post_section[$i][$j]['id_paragraph']) ?>" class="btn btn-primary">Modifier paragraphe</a>
                                <a href="<?= site_url('Paragraph/Delete/' . $post['id'] . '/' . $post_section[$i][$j]['id_paragraph']) ?>" class="btn btn-danger">Supprimer paragraphe</a>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <div class="strong-words">
                        <h5>
                            <?php
                            if (isset($post['strong_word'])) {
                                for ($i = 0; $i < count($post['strong_word']); $i++) { ?>
                                    <strong>#<?= $post['strong_word'][$i] ?></strong>

                            <?php if ($i != count($post['strong_word']) - 1) {
                                        echo ' ';
                                    }
                                }
                            } ?>
                        </h5>
                    </div>
                    <br>
                    <span class="meta">Ecrit par <?= $post['author'] ?>, publié le <?= date_format(date_create($post['date_post']), 'd/m/Y H:i:s') ?></span>

                </div>
            </div>
        </div>
    </article>

    <!-- Footer -->
    <?php include 'post-footer.php'; ?>

    <!-- Bootstrap core JavaScript -->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../../assets/js/clean-blog.min.js"></script>

</body>

</html>