<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/svg" href="../../assets/img/virus.svg" />
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alerte COVID-19 - Nouveau Post</title>
    <!-- Bootstrap core CSS -->
    <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="../../assets/css/clean-blog.min.css" rel="stylesheet">
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:rgb(49, 48, 48);" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="<?= base_url() ?>" style="padding: 5px; color: #fff;">
                <img src="../../assets/img/virus.svg" width="40px" height="40px" alt="bacteria">
                <span style="color:brown">
                    ALERTE </span>COVID-19</a>
            <button class="navbar-toggler navbar-toggler-right" style="color:#fff" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <?php include 'back-nav-ul.php'; ?>
            </div>
        </div>
    </nav>
    <div class="container">

        <div style="margin-top: 100px;margin-bottom:250px; height:100vh;">
            <div class="container">
                <?php if ($upload_message != '') { ?>
                    <div class="message-banner">
                        <p><?= $upload_message ?></p>
                    </div>
                <?php } ?>
            </div>
            <div class="row ">
                <div class="col-lg-4 col-md-8 col-sm-8 col-10 mx-auto ">
                    <h1 class="text-center">Formulaire Post</h1>
                    <div style="font-size: smaller;font-style:italic;text-align:center">Tous les champs avec (<span class="required">*</span>) sont obligatoires.</div>
                    <hr>
                    <form method="post" action="<?= site_url('Post/Update_Action') ?>">
                        <div class="form-group">
                            <label for="id_section">Section<span class="required">*</span></label>
                            <div class="form-"></div>
                            <select class="form-control" name="id_section" id="id_section">
                                <?php foreach ($sections as $s) { ?>
                                    <option value="<?= $s['id'] ?>" <?php if ($s['id'] == $post['id_section']) {
                                                                        echo ' selected';
                                                                    } ?>><?= $s['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Titre de l'article<span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="title" id="title" value="<?= $post['title'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="author">Auteur<span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="author" value="<?= $post['author'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="strong_words">Mots clés</label>
                            <div class="input-group">
                                <input type="text" class="form-control" value="<?= $post['strong_words'] ?>" name="strong_words">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="chapo">Chapo<span class="required">*</span></label>
                            <div class="input-group">
                                <textarea class="form-control" rows="3" name="chapo" required><?= $post['chapo'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="background_copyright">Image Copyright</label>
                            <div class="input-group">
                                <input type="text" class="form-control" value="<?= $post['background_copyright'] ?>" name="background_copyright">
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?= $post['id'] ?>">
                        <div class="form-group">
                            <button class="form-control btn-success" type="submit">Modifier</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript -->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../../assets/js/clean-blog.min.js"></script>

</body>

</html>