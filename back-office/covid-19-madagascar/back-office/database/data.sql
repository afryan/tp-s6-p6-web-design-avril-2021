INSERT INTO
    Section
VALUES
    (1, 'Sante');

INSERT INTO
    Section
VALUES
    (2, 'Politique');

INSERT INTO
    Section
VALUES
    (3, 'Reportage');

INSERT INTO
    Section
VALUES
    (4, 'Astuce');

INSERT INTO
    Post (
        id,
        id_section,
        title,
        author,
        strong_words,
        chapo
    )
VALUES
    (
        2,
        2,
        'Covid à Madagascar : la guerre de l’oxygène',
        'Jeune Afrique',
        'covid-19,madagascar',
        'Alors que les autorités viennent enfin de confirmer leur intérêt pour les vaccins, Madagascar subit une vague de Covid-19 plus virulente encore que celle de 2020. Les centres de santé sont déjà pleins. Et surtout, une pénurie d’oxygène provoque des décès évitables.'
    );

INSERT INTO
    PostSection(id, id_post, title)
VALUES
    (3, 2, '');

INSERT INTO
    PostSection(id, id_post, title)
VALUES
    (4, 2, "Mesures de l'Etat");

INSERT INTO
    PostParagraph(id, id_post_section, content)
VALUES
    (
        4,
        3,
        "Si le sifflement s’arrête, les malades savent qu’ils vont mourir. Si le bruit s’arrête, c’est que l’oxygène ne sort plus de la bouteille. Vide. C’est que l’oxygène ne pallie plus les défaillances des poumons des personnes atteintes d’une forme grave de Covid, les condamnant à étouffer lentement."
    );

INSERT INTO
    PostParagraph(id, id_post_section, content)
VALUES
    (
        5,
        4,
        "Pour absorber l’afflux de malades, des hôtels et des établissements scolaires ont été transformés en hôpitaux à Madagascar, submergé par une envolée des cas de Covid-19, liée à la propagation du variant sud-africain. Dans ces centres improvisés, les familles rencontrées par France 24 dénoncent des négligences, dont des pannes d’oxygène et des personnels sous-qualifiés."
    );

INSERT INTO
    User
VALUES
    (
        1,
        'admin',
        sha1('ituniversity'),
        'admin@admin.org'
    );

INSERT INTO
    Question
VALUES
    (
        1,
        "Qu'est ce que la COVID-19 ?",
        "Covid-19 fait référence à « Coronavirus Disease 2019 », la maladie provoquée par un virus de la famille des Coronaviridae, le SARS-CoV-2. Cette maladie infectieuse est une zoonose, dont l'origine est encore débattue, qui a émergé en décembre 2019 dans la ville de Wuhan, dans la province du Hubei en Chine."
    );

INSERT INTO
    Question
VALUES
    (
        2,
        "Quels sont les symptômes de la COVID-19 ?",
        "Les symtômes les plus fréquents sont : fièvre,toux sèche,fatigue.Les moins fréquents : courbatures, maux de gorge, diarrhée, conjonctivite, maux de tête, perte de l’odorat ou du goût, éruption cutanée, ou décoloration des doigts ou des orteils.Les symtômes graves : difficultés à respirer ou essoufflement, sensation d’oppression ou douleur au niveau de la poitrine, perte d’élocution ou de motricité.
"
    );

INSERT INTO
    Question
VALUES
    (
        3,
        "Existe-t-il un médicament contre la COVID-19 ?",
        "Non, les virus ne soignent pas avec des médicament mais avec des vaccins."
    );

INSERT INTO
    Question
VALUES
    (
        4,
        "Existe-t-il un vaccin contre la COVID-19 ?",
        "Oui, plusieurs pays ont créé un vaccin contre la COVID-19 mais aucun n'est efficace à 100%."
    );

INSERT INTO
    Question
VALUES
    (
        5,
        "Que faire si on a les symptômes de la COVID-19 ?",
        "Oui, plusieurs pays ont créé un vaccin contre la COVID-19 mais à ce jour, aucun n'est efficace à 100%."
    );