CREATE DATABASE db_covid_blog;

CREATE TABLE Section(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    PRIMARY KEY(id)
);

CREATE TABLE Post(
    id INT NOT NULL AUTO_INCREMENT,
    id_section INT NOT NULL,
    title VARCHAR(255) NOT NULL,
    author VARCHAR(255) NOT NULL,
    strong_words VARCHAR(255),
    chapo VARCHAR(500) NOT NULL,
    background_url VARCHAR(255),
    background_copyright VARCHAR(255),
    date_post TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY(id_section) REFERENCES Section(id)
);

CREATE TABLE PostSection(
    id INT NOT NULL AUTO_INCREMENT,
    id_post INT NOT NULL,
    title VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE(id, id_post),
    FOREIGN KEY(id_post) REFERENCES Post(id)
);

CREATE TABLE PostParagraph(
    id INT NOT NULL AUTO_INCREMENT,
    id_post_section INT NOT NULL,
    content VARCHAR(500) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE (id, id_post_section),
    FOREIGN KEY(id_post_section) REFERENCES PostSection(id)
);

CREATE TABLE User(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(20) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(50) NOT NULL,
    PRIMARY KEY(id),
    INDEX(username)
);

CREATE TABLE Question(
    id INT NOT NULL AUTO_INCREMENT,
    question VARCHAR(255) NOT NULL,
    response VARCHAR(1000) NOT NULL,
    PRIMARY KEY(id)
);