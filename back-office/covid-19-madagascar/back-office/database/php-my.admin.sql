-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: May 04, 2021 at 08:06 AM
-- Server version: 5.7.32-log
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `db_covid_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `Post`
--

CREATE TABLE `Post` (
  `id` int(11) NOT NULL,
  `id_section` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `strong_words` varchar(255) DEFAULT NULL,
  `chapo` varchar(500) NOT NULL,
  `background_url` varchar(255) DEFAULT NULL,
  `background_copyright` varchar(255) DEFAULT NULL,
  `date_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Post`
--

INSERT INTO `Post` (`id`, `id_section`, `title`, `author`, `strong_words`, `chapo`, `background_url`, `background_copyright`, `date_post`) VALUES
(2, 2, 'Covid à Madagascar : la guerre de l\'oxygène', 'Jeune Afrique', 'covid-19,Madagascar,Covid,Covid-19', 'Alors que les autorités viennent enfin de confirmer leur intérêt pour les vaccins, Madagascar subit une vague de Covid-19 plus virulente encore que celle de 2020. Les centres de santé sont déjà pleins. Et surtout, une pénurie d’oxygène provoque des décès évitables.', 'oxygene-madagascar.jpeg', 'UNICEF', '2021-04-29 16:29:44'),
(4, 2, 'Covid-19 : L\'Etat Malgache annonce un confinement total pour les week-end à Antananarivo', 'Admin', 'Covid,Madagascar', 'Dans la capitale de la grande Ile, les nouveaux cas, les formes graves et les décès ne cessent de se multiplier. Pas en mesure d\'affronter un confinement total, l\'Etat opte pour un confinement total applicable uniquement pour les weekend.', '', '', '2021-05-01 07:05:02'),
(20, 1, 'Coronavirus en Inde : plus de 200 000 décès en un jour', 'Admin', 'Coronavirus', 'Le coronavirus ne cesse de faire des victimes en Inde depuis quelques semaines. Hier encore, plus de 200 000 ont été recensé et plus de 20 Millions de cas positifs cumulés dorénavant.', '1620113651-covid-inde.jpeg', 'Le Monde', '2021-05-04 07:34:11');

-- --------------------------------------------------------

--
-- Table structure for table `PostParagraph`
--

CREATE TABLE `PostParagraph` (
  `id` int(11) NOT NULL,
  `id_post_section` int(11) NOT NULL,
  `content` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PostParagraph`
--

INSERT INTO `PostParagraph` (`id`, `id_post_section`, `content`) VALUES
(4, 3, 'Si le sifflement s’arrête, les malades savent qu’ils vont mourir. Si le bruit s’arrête, c’est que l’oxygène ne sort plus de la bouteille. Vide. C’est que l’oxygène ne pallie plus les défaillances des poumons des personnes atteintes d’une forme grave de Covid, les condamnant à étouffer lentement.'),
(5, 4, 'Pour absorber l’afflux de malades, des hôtels et des établissements scolaires ont été transformés en hôpitaux à Madagascar, submergé par une envolée des cas de Covid-19, liée à la propagation du variant sud-africain. Dans ces centres improvisés, les familles rencontrées par France 24 dénoncent des négligences, dont des pannes d’oxygène et des personnels sous-qualifiés.'),
(19, 9, 'Comme toujours, la politique n\'est jamais mis à l\'écart à Madagascar. Chaque décision doit être étudiée dans tous les aspects par les experts avant d\'être appliquée ... toujours dans le but de restreindre les cas Covid.'),
(21, 11, 'Comme on le sait, le Coronavirus se propage très facilement et l\'Inde a une forte densité de population. Le gouvernement a sa part de responsabilité mais la variance du virus a été dévastatrice, surtout au manque de personnels et matériels sanitaires.');

-- --------------------------------------------------------

--
-- Table structure for table `PostSection`
--

CREATE TABLE `PostSection` (
  `id` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PostSection`
--

INSERT INTO `PostSection` (`id`, `id_post`, `title`) VALUES
(3, 2, ''),
(4, 2, 'Mesures de l\'Etat'),
(9, 4, 'Politique ou Sanitaire ?'),
(11, 20, 'Mauvaise politique anti-covid ou lacune sanitaire ?');

-- --------------------------------------------------------

--
-- Table structure for table `Question`
--

CREATE TABLE `Question` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `response` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Question`
--

INSERT INTO `Question` (`id`, `question`, `response`) VALUES
(1, 'Qu\'est ce que la COVID-19 ?', 'Covid-19 fait référence à « Coronavirus Disease 2019 », la maladie provoquée par un virus de la famille des Coronaviridae, le SARS-CoV-2. Cette maladie infectieuse est une zoonose, dont l\'origine est encore débattue, qui a émergé en décembre 2019 dans la ville de Wuhan, dans la province du Hubei en Chine.'),
(2, 'Quels sont les symptômes de la COVID-19 ?', 'Les symtômes les plus fréquents sont : fièvre,toux sèche,fatigue.Les moins fréquents : courbatures, maux de gorge, diarrhée, conjonctivite, maux de tête, perte de l’odorat ou du goût, éruption cutanée, ou décoloration des doigts ou des orteils.Les symtômes graves : difficultés à respirer ou essoufflement, sensation d’oppression ou douleur au niveau de la poitrine, perte d’élocution ou de motricité.\r\n'),
(3, 'Existe-t-il un médicament contre la COVID-19 ?', 'Non, les virus ne soignent pas avec des médicament mais avec des vaccins.'),
(4, 'Existe-t-il un vaccin contre la COVID-19 ?', 'Oui, plusieurs pays ont créé un vaccin contre la COVID-19 mais aucun n\'est à ce jour efficace à 100%.\r\n<br>Celà est dû aux différentes variantes de la COVID-19.'),
(5, 'Que faire si on a les symptômes de la COVID-19 ?', 'Il faut en premier temps s\'auto-confiner pour éviter de propager le virus, sauf en cas de complication.<br>\r\nDans ce cas là, appeler le 913 et/ou approcher les CSBII pour obtenir les médicaments permettant de traiter les symptômes.\r\n<br>Si l\'état se met encore à s\'aggraver, contactez le centre d\'appel du CTC-19 pour qu\'ils puissent vous orienter vers le CTC-19 le plus adapté à votre cas.');

-- --------------------------------------------------------

--
-- Table structure for table `Section`
--

CREATE TABLE `Section` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Section`
--

INSERT INTO `Section` (`id`, `name`) VALUES
(4, 'Astuce'),
(2, 'Politique'),
(3, 'Reportage'),
(1, 'Sante');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', '1add1f782783f824b8ced34db65dca83b538e2b4', 'admin@admin.org'),
(3, 'test', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test@test.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Post`
--
ALTER TABLE `Post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_section` (`id_section`);

--
-- Indexes for table `PostParagraph`
--
ALTER TABLE `PostParagraph`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`,`id_post_section`),
  ADD KEY `id_post_section` (`id_post_section`);

--
-- Indexes for table `PostSection`
--
ALTER TABLE `PostSection`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`,`id_post`),
  ADD KEY `id_post` (`id_post`);

--
-- Indexes for table `Question`
--
ALTER TABLE `Question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Section`
--
ALTER TABLE `Section`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_2` (`username`),
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Post`
--
ALTER TABLE `Post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `PostParagraph`
--
ALTER TABLE `PostParagraph`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `PostSection`
--
ALTER TABLE `PostSection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `Question`
--
ALTER TABLE `Question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `Section`
--
ALTER TABLE `Section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Post`
--
ALTER TABLE `Post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`id_section`) REFERENCES `Section` (`id`);

--
-- Constraints for table `PostParagraph`
--
ALTER TABLE `PostParagraph`
  ADD CONSTRAINT `postparagraph_ibfk_1` FOREIGN KEY (`id_post_section`) REFERENCES `PostSection` (`id`);

--
-- Constraints for table `PostSection`
--
ALTER TABLE `PostSection`
  ADD CONSTRAINT `postsection_ibfk_1` FOREIGN KEY (`id_post`) REFERENCES `Post` (`id`);
