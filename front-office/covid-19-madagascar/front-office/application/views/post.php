<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/svg" href="../assets/img/virus.svg" />

  <meta name="description" content="<?= $post['chapo'] ?>">
  <meta name="author" content="ETU01068">

  <title>Alerte COVID-19 - <?= $post['title'] ?></title>

  <?php include 'post-style-links.php'; ?>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:rgb(49, 48, 48);" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="<?= base_url() ?>" style="padding: 5px; color: #fff;">
        <img src="../assets/img/virus.svg" width="40px" height="40px" alt="bacteria">
        <span style="color:brown">
          ALERTE </span>COVID-19</a>
      <button class="navbar-toggler navbar-toggler-right" style="color:#fff" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <?php include 'nav-bar-link.php'; ?>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-color:#fff">
    <div class="overlay" style="background-color: #fff;"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading" style="padding: 100px 0 20px;">
            <h1 class="article-title" style="color:#000;font-weight:900;"><?= $post['title'] ?></h1>
            <?php if (isset($post['background_url'])) { ?>
              <img src="<?= $back_url ?>/assets/img/<?= $post['background_url'] ?>" alt="<?= $post['background_url'] ?>" width="100%" height="100%">
              <?php if (isset($post['background_copyright'])) { ?>
                <span class="copyright" style="font-size: small; color:gray;">&copy; <?= $post['background_copyright'] ?></span>
              <?php } ?>
            <?php } ?>
          </div>
          <h3 class="post-subtitle chapo" style="font-weight:bold;"><?= $post['chapo'] ?></h3>
        </div>
      </div>
    </div>
  </header>

  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <?php for ($i = 0; $i < count($post_section); $i++) {
            for ($j = 0; $j < count($post_section[$i]); $j++) {
              if ($j == 0) { ?>
                <h2 class="section-heading"><?= $post_section[$i][$j]['title'] ?></h2>
              <?php } ?>


              <p style="font-family:'Open Sans',Arial,sans-serif;"><?= $post_section[$i][$j]['content'] ?></p>
          <?php     }
          } ?>
          <div class="strong-words">
            <h5>
              <?php
              if (isset($post['strong_word'])) {
                for ($i = 0; $i < count($post['strong_word']); $i++) { ?>
                  <strong>#<?= $post['strong_word'][$i] ?></strong>

              <?php if ($i != count($post['strong_word']) - 1) {
                    echo ' ';
                  }
                }
              } ?>
            </h5>
          </div>
          <br>
          <span class="meta">Ecrit par <?= $post['author'] ?>, publié le <?= date_format(date_create($post['date_post']), 'd/m/Y H:i:s') ?></span>

        </div>
      </div>
    </div>
  </article>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <hr>
        <h4>Voir aussi ...</h4>
        <?php for ($i = 0; $i < count($recent_post); $i++) { ?>
          <div class="post-preview ">
            <a href="<?= site_url(('/' . $recent_post[$i]['post_url'])) ?>" class="post">
              <div class="post-category"><?= $recent_post[$i]['section_name'] ?></div>

              <h2 class="post-title ">
                <?= $recent_post[$i]['title'] ?>
              </h2>
              <h3 class="post-subtitle chapo">
                <?= $recent_post[$i]['chapo'] ?>
              </h3>
              <span class="meta">Ecrit par <?= $recent_post[$i]['author'] ?>, publié le <?= date_format(date_create($recent_post[$i]['date_post']), 'd/m/Y H:i:s') ?></span>
            </a>
          </div>
        <?php if ($i != count($recent_post) - 1) {
            echo '<hr>';
          }
        } ?>
      </div>
    </div>
  </div>


  <!-- Footer -->
  <?php include 'post-footer.php'; ?>

  <!-- Bootstrap core JavaScript -->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="../assets/js/clean-blog.min.js"></script>

</body>

</html>