<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="1er site d'actualité spécialisées dans la COVID-19 à Madagascar.">
	<link rel="icon" type="image/svg" href="assets/img/virus.svg" />

	<meta name="author" content="ETU001068">

	<title><?=$message?></title>
	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom fonts for this template -->
	<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

	<!-- Custom styles for this template -->
	<link href="assets/css/clean-blog.min.css" rel="stylesheet">
</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:rgb(49, 48, 48);" id="mainNav">
		<div class="container">
			<a class="navbar-brand" href="<?=''?>" style="padding: 5px; color: #fff;">
				<img src="assets/img/virus.svg" width="40px" height="40px" alt="bacteria">
				<span style="color:brown">
					ALERTE </span>COVID-19</a>
			<button class="navbar-toggler navbar-toggler-right" style="color:#fff" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="<?='a-propos-covid'?>">COVID-19</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- Main Content -->
	<div class="container ">
		<div class="row ">
			<div class="col-lg-8 col-md-10 mx-auto" style="margin-top: 120px ">
				<p class="text-center"><img src="assets/img/error-404.png" alt="error-404" width="200px" height="200px"></p>
				<h3 style="text-align:center"class="error"><?= $message ?></h3>
				<hr>
				<h3>Voir aussi ...</h3>
				<?php for ($i = 0; $i < count($recent_post); $i++) { ?>
					<div class="post-preview ">
						<a href="<?= site_url(('/' . $recent_post[$i]['post_url'])) ?>" class="post">
							<div class="post-category"><?= $recent_post[$i]['section_name'] ?></div>

							<h2 class="post-title">
								<?= $recent_post[$i]['title'] ?>
							</h2>
							<h3 class="post-subtitle ">
								<?= $recent_post[$i]['chapo'] ?>
							</h3>
						</a>
					</div>

				<?php if ($i != count($recent_post) - 1) {
						echo '<hr>';
					}
				} ?>
			</div>
		</div>
	</div>
	<footer style="background-color: rgb(49, 48, 48); ">
        <div class="container ">
            <div class="row ">
                <div class="col-lg-8 col-md-10 mx-auto ">
                    <p class="copyright" style="color: #fff;">Copyright &copy; ETU001068 - Andriamahery Fihariantsoa Ryan <br>Images & icons credits : flaticon.com</p>
                </div>
            </div>
        </div>
    </footer>
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js "></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js "></script>

	<!-- Custom scripts for this template -->
	<script src="assets/js/clean-blog.min.js "></script>

</body>

</html>