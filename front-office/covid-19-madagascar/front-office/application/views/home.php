<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/svg" href="assets/img/virus.svg" />
    <meta name="description" content="1er site d'actualité spécialisées dans la COVID-19 à Madagascar.">
    <meta name="author" content="ETU001068">

    <title>Alerte COVID-19 - Informations et actualités liées à la Covid à Madagascar et dans le monde.</title>
    <?php include 'style-links.php'; ?>
</head>

<body>

    <?php include 'nav-bar.php'; ?>

    <!-- Page Header -->
    <header class="masthead home">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="post-heading">
                        <h1>Informations et actualités liées à la Covid à Madagascar et dans le monde.</h1>
                        <h2 class=" subheading">
                            1er site d'<strong>actualités</strong> spécialisées dans la <strong>COVID-19</strong> à Madagascar.
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <!-- Main Content -->
    <div class="container ">
        <div class="row ">
            <div class="col-lg-8 col-md-10 mx-auto ">
                <?php for ($i = 0; $i < count($recent_post); $i++) { ?>
                    <div class="post-preview">
                        <a href="<?= site_url(('/' . $recent_post[$i]['post_url'])) ?>" class="post">
                            <div class="post-category"><?= $recent_post[$i]['section_name'] ?></div>

                            <h2 class="post-title ">
                                <?= $recent_post[$i]['title'] ?>
                            </h2>
                            <h3 class="post-subtitle ">
                                <?= $recent_post[$i]['chapo'] ?>
                            </h3>
                        </a>
                        <span class="meta">Ecrit par <?=$recent_post[$i]['author']?>, publié le <?=date_format(date_create($recent_post[$i]['date_post']),'d/m/Y H:i:s')?></span>
                    </div>

                <?php if ($i != count($recent_post) - 1) {
                        echo '<hr>';
                    }
                } ?>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <?php include 'footer.php'; ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js "></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js "></script>

    <!-- Custom scripts for this template -->
    <script src="assets/js/clean-blog.min.js "></script>

</body>

</html>