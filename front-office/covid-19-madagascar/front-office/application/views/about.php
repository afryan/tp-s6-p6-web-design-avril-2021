<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/svg" href="assets/img/virus.svg" />
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alerte COVID-19 - A propos</title>
    <?php include 'style-links.php'; ?>

</head>

<body>

    <?php include 'nav-bar.php'; ?>

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('assets/img/question.svg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="page-heading">
                        <h1>Des questions sur la COVID-19 ?</h1>
                        <span class="subheading">Nous sommes là pour vous répondre.</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <?php foreach ($question as $q) { ?>
                    <div class="question">
                        <h2><?= $q['question'] ?></h2>
                        <p><?= $q['response'] ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <hr>

    <!-- Footer -->
    <?php include 'footer.php'; ?>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/clean-blog.min.js"></script>

</body>

</html>