<?php
defined('BASEPATH') or exit('No direct script access allowed');
class PostModel extends CI_Model
{
    public function find_post_by_ids($id, $id_section)
    {
        $post = $this->db->get_where('Post', ['id' => $id, 'id_section' => $id_section])->row_array();
        if (isset($post)) {
            $post['strong_word'] = $post['strong_words'] == '' ? null : explode(",",  $post['strong_words']);
            $post['background_url'] = $post['background_url'] == '' ? null :  $post['background_url'];
            $post['background_copyright'] = $post['background_copyright'] == '' ? null :  $post['background_copyright'];
        }
        return $post;
    }
    public function find_post_by_id($id)
    {
        $post = $this->db->get_where('Post', ['id' => $id])->row_array();

        if (isset($post)) {

            $post['strong_word'] = $post['strong_words'] == '' ? null : explode(",",  $post['strong_words']);
            $post['background_url'] = $post['background_url'] == '' ? null :  $post['background_url'];
            $post['background_copyright'] = $post['background_copyright'] == '' ? null :  $post['background_copyright'];
        }

        return $post;
    }
    public function find_recent_post($id)
    {
        $sql = sprintf("SELECT p.*,name AS section_name FROM Post p JOIN Section s ON p.id_section = s.id WHERE p.id != %d ORDER BY date_post DESC LIMIT 3", $id);
        $array = $this->db->query($sql)->result_array();
        for ($i = 0; $i < count($array); $i++) {
            $array[$i]['post_url'] = strtolower($array[$i]['section_name']) . "/article-" . str_replace(array(" "), "-",  str_replace(array("'", '"', " +", " -", " /", " : ", ";"), ' ', strtolower($array[$i]['title']))) . "-" . $array[$i]['id_section'] . "-" . $array[$i]['id'];
        }
        return $array;
    }

    public function find_last_post()
    {
        $sql = sprintf("SELECT p.*,name AS section_name FROM Post p JOIN Section s ON p.id_section = s.id ORDER BY p.id DESC LIMIT 1");
        $array = $this->db->query($sql)->row_array();
        $array['post_url'] = strtolower($array['section_name']) . "/article-" . str_replace(array(" "), "-",  str_replace(array("'", '"', " +", " -", " /", " : ", ";"), ' ', strtolower($array['title']))) . "-" . $array['id_section'] . "-" . $array['id'];

        return $array;
    }

    public function count_post()
    {
        $sql = "SELECT COUNT(*) AS count_post FROM Post";
        return $this->db->query($sql)->row_array();
    }

    public function find_all_post_by_date_desc($page, $limit)
    {
        if ($page == 1) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * $limit;
        }
        $sql = sprintf("SELECT p.*,name AS section_name FROM Post p JOIN Section s ON p.id_section = s.id ORDER BY date_post DESC LIMIT %d OFFSET %d", $limit, $offset);
        $array = $this->db->query($sql)->result_array();
        for ($i = 0; $i < count($array); $i++) {
            $array[$i]['post_url'] = strtolower($array[$i]['section_name']) . "/article-" . str_replace(array(" "), "-",  str_replace(array("'", '"', " +", " -", " /", " : ", ";"), ' ', strtolower($array[$i]['title']))) . "-" . $array[$i]['id_section'] . "-" . $array[$i]['id'];
        }
        return $array;
    }


    public function find_all_recent_post($limit)
    {
        $sql = sprintf("SELECT p.*,name AS section_name FROM Post p JOIN Section s ON p.id_section = s.id ORDER BY date_post DESC LIMIT %d", $limit);
        $array = $this->db->query($sql)->result_array();
        for ($i = 0; $i < count($array); $i++) {
            $array[$i]['post_url'] = strtolower($array[$i]['section_name']) . "/article-" . str_replace(array(" "), "-",  str_replace(array("'", '"', " +", " -", " /", " : ", ";"), ' ', strtolower($array[$i]['title']))) . "-" . $array[$i]['id_section'] . "-" . $array[$i]['id'];
        }
        return $array;
    }

    public function find_last_post_section()
    {
        $sql = "SELECT * FROM PostSection ORDER BY id DESC LIMIT 1";
        return $this->db->query($sql)->result_array();
    }
    public function find_post_sections_by_id($id_post)
    {
        $sql = sprintf("SELECT ps.* FROM Post p JOIN PostSection ps ON ps.id_post = p.id where p.id = %d  ORDER BY ps.id ASC", $id_post);
        return $this->db->query($sql)->result_array();
    }
    public function find_post_sections_by_ids($id_post, $id_section)
    {
        $sql = sprintf("SELECT ps.* FROM Post p RIGHT JOIN PostSection ps ON ps.id_post = p.id where p.id = %d AND p.id_section = %d ORDER BY ps.id ASC", $id_post, $id_section);
        return $this->db->query($sql)->result_array();
    }
    public function find_post_paragraphs_by_ids($id_post, $id_section)
    {
        $sql = sprintf("SELECT ps.*,pp.* FROM PostSection ps JOIN PostParagraph pp ON ps.id = pp.id_post_section JOIN Post p ON p.id = ps.id_post where p.id = %d AND p.id_section = %d ORDER BY p.id ASC", $id_post, $id_section);
        return $this->db->query($sql)->result_array();
    }

    public function find_post_paragraphs_by_id($id_post)
    {
        $sql = sprintf("SELECT ps.*,pp.* FROM PostSection ps JOIN PostParagraph pp ON ps.id = pp.id_post_section JOIN Post p ON p.id = ps.id_post WHERE p.id = %d ORDER BY p.id ASC", $id_post);
        return $this->db->query($sql)->result_array();
    }

    public function find_post_section_by_id($id_post_section)
    {
        $sql = sprintf("SELECT * FROM PostSection WHERE id = %d", $id_post_section);
        return $this->db->query($sql)->row_array();
    }

    public function find_post_paragraph_by_id($id_paragraph)
    {
        $sql = sprintf("SELECT * FROM PostParagraph WHERE id = %d", $id_paragraph);
        return $this->db->query($sql)->row_array();
    }

    public function find_all_post_section_paragraphs_by_ids($id, $id_section)
    {
        $sections_array = $this->find_post_sections_by_ids($id, $id_section);
        $result = array();
        for ($i = 0; $i < count($sections_array); $i++) {
            $result[$i] = $this->find_post_section_paragraphs_by_ids($id, $id_section, $sections_array[$i]['id']);
        }
        return $result;
    }

    public function find_all_post_section_paragraphs_by_id($id)
    {
        $sections_array = $this->find_post_sections_by_id($id);
        $result = array();
        for ($i = 0; $i < count($sections_array); $i++) {
            $result[$i] = $this->find_post_section_paragraphs_by_id($id, $sections_array[$i]['id']);
        }
        return $result;
    }


    public function find_post_section_paragraphs_by_ids($id, $id_section, $id_post_section)
    {
        $post_section = $this->find_post_section_by_ids($id, $id_section, $id_post_section);
        $paragraph_array = $this->find_post_paragraphs_by_ids($id, $id_section);
        $post = $this->find_post_by_ids($id, $id_section);
        $strong_words = explode(",", $post['strong_words']);

        $result = array();

        $tmp = 0;
        $count = 0;
        for ($i = 0; $i < count($paragraph_array); $i++) {
            if ($post_section['id'] == $paragraph_array[$i]['id_post_section']) {
                $count++;
            }
        }
        for ($i = 0; $i < count($paragraph_array); $i++) {
            if ($post_section['id'] == $paragraph_array[$i]['id_post_section']) {
                $result[$tmp]['title'] = $post_section['title'];
                $content = $paragraph_array[$i]['content'];
                foreach ($strong_words as $w) {
                    $content = str_replace($w, "<strong>" . $w . "</strong>", $content);
                }
                $result[$tmp]['content'] = $content;
                $tmp++;
            }
        }

        return $result;
    }
    public function find_post_section_by_id_post($id, $id_post_section)
    {
        $sql = sprintf("SELECT ps.* FROM Post p RIGHT JOIN PostSection ps ON ps.id_post = p.id where p.id = %d AND ps.id = %d ORDER BY ps.id ASC", $id, $id_post_section);
        return $this->db->query($sql)->row_array();
    }
    public function find_post_section_paragraphs_by_id($id_post, $id)
    {
        $post_section = $this->find_post_section_by_id_post($id_post, $id);
        $paragraph_array = $this->find_post_paragraphs_by_id($id_post);
        $post = $this->find_post_by_id($id_post);
        $strong_words = explode(",", $post['strong_words']);

        $result = array();

        $tmp = 0;
        $count = 0;
        for ($i = 0; $i < count($paragraph_array); $i++) {
            if ($post_section['id'] == $paragraph_array[$i]['id_post_section']) {
                $count++;
            }
        }
        $result[$tmp]['title'] = $post_section['title'];
        $result[$tmp]['id_section'] = $post_section['id'];
        $result[$tmp]['content'] = '';

        for ($i = 0; $i < count($paragraph_array); $i++) {
            if ($post_section['id'] == $paragraph_array[$i]['id_post_section']) {

                $result[$tmp]['id_paragraph'] = $paragraph_array[$i]['id'];
                $content = $paragraph_array[$i]['content'];
                foreach ($strong_words as $w) {
                    $content = str_replace($w, "<strong>" . $w . "</strong>", $content);
                }
                $result[$tmp]['content'] = $content;
                $tmp++;
            }
        }


        return $result;
    }

    public function find_post_section_by_ids($id, $id_section, $id_post_section)
    {
        $sql = sprintf("SELECT ps.* FROM Post p JOIN PostSection ps ON ps.id_post = p.id where p.id = %d AND p.id_section = %d  AND ps.id = %d ORDER BY ps.id ASC", $id, $id_section, $id_post_section);
        return $this->db->query($sql)->row_array();
    }

    public function create_post($id_section, $title, $author, $strong_words, $chapo, $background_url, $background_copyright)
    {
        $sql = sprintf("INSERT INTO Post (id_section,title,author,strong_words,chapo,background_url,background_copyright) VALUES (%d,\"%s\",'%s',\"%s\",\"%s\",'%s','%s')", $id_section, $title, $author, $strong_words, $chapo, $background_url, $background_copyright);
        return $this->db->query($sql);
    }
    public function update_post($id, $id_section, $title, $author, $strong_words, $chapo, $background_copyright)
    {
        $sql = sprintf("UPDATE Post SET id_section = %d , title=\"%s\", author = '%s', strong_words = '%s', chapo = \"%s\",background_copyright = '%s' WHERE id = %d", $id_section, $title, $author, $strong_words, $chapo, $background_copyright, $id);
        return $this->db->query($sql);
    }
    public function create_post_section($id_post, $title)
    {
        $sql = sprintf("INSERT INTO PostSection (id_post,title) VALUES (%d,\"%s\")", $id_post, $title);
        return $this->db->query($sql);
    }

    public function update_post_section($id, $title)
    {
        $sql = sprintf("UPDATE PostSection SET title = \"%s\" WHERE id = %d", $title, $id);
        return $this->db->query($sql);
    }

    public function create_post_paragraph($id_post_section, $content)
    {
        $sql = sprintf("INSERT INTO PostParagraph (id_post_section,content) VALUES (%d,\"%s\")", $id_post_section, $content);
        return $this->db->query($sql);
    }

    public function update_post_paragraph($id, $content)
    {
        $sql = sprintf("UPDATE PostParagraph SET content = \"%s\" WHERE id = %d",  $content, $id);
        return $this->db->query($sql);
    }
    public function delete_paragraph($id_paragraph)
    {
        $sql = sprintf("DELETE FROM PostParagraph WHERE id = %d", $id_paragraph);
        return $this->db->query($sql);
    }
    public function delete_section($id_section)
    {
        $sql = sprintf("DELETE FROM PostSection WHERE id = %d", $id_section);
        return $this->db->query($sql);
    }
    public function delete_post($id_post)
    {
        $sql = sprintf("DELETE FROM Post WHERE id = %d", $id_post);
        return $this->db->query($sql);
    }
    public function delete_post_section($id)
    {
        $sql = sprintf("DELETE FROM PostSection WHERE id = %d", $id);
        return $this->db->query($sql);
    }
    public function delete_post_rec($id_post)
    {
        $paragraphs = $this->find_post_paragraphs_by_id($id_post);
        foreach ($paragraphs as $p) {
            $this->delete_paragraph($p['id']);
        }
        $section = $this->find_post_sections_by_id($id_post);
        foreach ($section as $s) {
            $this->delete_section($s['id']);
        }
        $this->delete_post($id_post);
    }
    public function find_paragraph_by_section($id_post_section)
    {
        $sql = sprintf("SELECT pp.* FROM PostSection ps JOIN PostParagraph pp ON ps.id = pp.id_post_section WHERE ps.id = %d", $id_post_section);
        return $this->db->query($sql)->result_array();
    }
    public function delete_post_section_rec($id)
    {
        $paragraphs = $this->find_paragraph_by_section($id);
        foreach ($paragraphs as $p) {
            $this->delete_paragraph($p['id']);
        }
        $this->delete_post_section($id);
    }
}
