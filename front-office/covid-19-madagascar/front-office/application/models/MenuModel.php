<?php
defined('BASEPATH') or exit('No direct script access allowed');
class MenuModel  extends CI_Model
{
    public function find_all_menu()
    {
        return $this->db->get('MenuCategory')->result_array();
    }
    public function add_menu($lien, $description)
    {
        $sql = sprintf("INSERT INTO MenuCategory(lien,description) VALUES ('%s','%s')", $lien, $description);
        return $this->db->query($sql);
    }
    public function change_menu($lien, $description)
    {
        $this->add_menu($lien, $description);

        if (file_exists('../views/nav-bar-generated.php')) {
            unlink('../views/nav-bar-generated.php');
        }
    }
    public function write_menu()
    {
        $nav_menu = $this->find_all_menu();
        $nav = fopen("../nav-bar-generated.php", "w");
        $nav_content = '<nav class=\"navbar navbar-expand-lg navbar-light fixed-top" id="mainNav"><div class="container"><a class="navbar-brand" href="<?= base_url() ?>" style="padding: 5px; color: #fff;"><img src="assets/img/virus.svg" width="40px" height="40px" alt="bacteria"><span style="color:brown">
                    ALERTE </span>COVID-19</a><button class="navbar-toggler navbar-toggler-right" style="color:#fff" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i></button><div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">';
        foreach ($nav_menu as $m) {
            $nav_content .= '<li class="nav-item"><a class="nav-link" href="' . $m['lien'] . '">' . $m['description'] . '</a></li>';
        }
        $nav_content .= ' </ul>
        </div>
    </div>
</nav>';
        fwrite($nav, $nav_content);
        fclose($nav);
    }
    public function get_menu()
    {
        if (!file_exists('../views/nav-bar-generated.php')) {
            $this->write_menu();
        }
        return 'nav-bar-generated.php';
    }
}
