<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function index()
	{
		$this->load->model('PostModel');
		// $this->load->model('MenuModel');
		$input = $this->input->get();
		$page = !isset($input['page']) ? 1 : $input['page'];
		$limit = !isset($input['limit']) ? 10 : $input['limit'];
		// $data['menu_url']=$this->MenuModel->get_menu();
		$data['recent_post'] = $this->PostModel->find_all_post_by_date_desc($page, $limit);
		if (count($data['recent_post']) == 0) {
			$error['heading'] = "404";
			$error['message'] = "Page non trouvée";
			$error['recent_post'] = $this->PostModel->find_all_recent_post(3);
			$this->load->view('errors/html/error_404.php', $error);
		} else {
			$this->load->view('home', $data);
		}
	}
}
