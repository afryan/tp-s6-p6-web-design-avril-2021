<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('production');
	}
	public function index($id_section, $id)
	{
		$this->load->model('PostModel');
		$data['post_section'] = $this->PostModel->find_all_post_section_paragraphs_by_ids($id, $id_section);
		$data['post'] = $this->PostModel->find_post_by_ids($id, $id_section);
		$data['recent_post'] = $this->PostModel->find_recent_post($id);
		$data['back_url']=$this->production->back_url();
		$this->load->view('post', $data);
	}
}
