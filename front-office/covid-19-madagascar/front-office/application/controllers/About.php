<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About extends CI_Controller
{
	public function index()
	{
		$this->load->model('QuestionModel');

		$data['question'] = $this->QuestionModel->find_question_response();
		$this->load->view('about', $data);
	}
}
