# TP S6 P6 Web design Avril 2021
# Thème : COVID-19 dans le monde et à Madagascar

# Auteur : 
- ETU 001068
- P12 A N°24
- Andriamahery Fihariantsoa Ryan

# Liens :
- Lien OPTIMISATION SITE :  https://docs.google.com/document/d/1ZEdseDnGema-N2JFdIHOIkmIy7allWrkszQM0wTT3PI/edit?usp=sharing
- Front-office : https://covid-19-fo.alwaysdata.net
- Back-office : https://covid-19-bo.alwaysdata.net
- Identifiant Back-office : admin
- Mot de passe Back-office : ituniversity